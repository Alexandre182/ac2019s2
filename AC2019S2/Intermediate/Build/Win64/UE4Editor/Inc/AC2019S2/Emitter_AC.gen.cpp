// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AC2019S2/Emitter_AC.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEmitter_AC() {}
// Cross Module References
	AC2019S2_API UClass* Z_Construct_UClass_AEmitter_AC_NoRegister();
	AC2019S2_API UClass* Z_Construct_UClass_AEmitter_AC();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_AC2019S2();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	AC2019S2_API UClass* Z_Construct_UClass_AParticle_NoRegister();
// End Cross Module References
	void AEmitter_AC::StaticRegisterNativesAEmitter_AC()
	{
	}
	UClass* Z_Construct_UClass_AEmitter_AC_NoRegister()
	{
		return AEmitter_AC::StaticClass();
	}
	struct Z_Construct_UClass_AEmitter_AC_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Particle_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Particle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEmitter_AC_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_AC2019S2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEmitter_AC_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Emitter_AC.h" },
		{ "ModuleRelativePath", "Emitter_AC.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Particle_MetaData[] = {
		{ "Category", "Emission" },
		{ "ModuleRelativePath", "Emitter_AC.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Particle = { "Particle", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEmitter_AC, Particle), Z_Construct_UClass_AParticle_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Particle_MetaData, ARRAY_COUNT(Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Particle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Count_MetaData[] = {
		{ "Category", "Emission" },
		{ "ModuleRelativePath", "Emitter_AC.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEmitter_AC, Count), METADATA_PARAMS(Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Count_MetaData, ARRAY_COUNT(Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AEmitter_AC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Particle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEmitter_AC_Statics::NewProp_Count,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEmitter_AC_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEmitter_AC>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEmitter_AC_Statics::ClassParams = {
		&AEmitter_AC::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AEmitter_AC_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AEmitter_AC_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AEmitter_AC_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AEmitter_AC_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEmitter_AC()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEmitter_AC_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEmitter_AC, 2202147401);
	template<> AC2019S2_API UClass* StaticClass<AEmitter_AC>()
	{
		return AEmitter_AC::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEmitter_AC(Z_Construct_UClass_AEmitter_AC, &AEmitter_AC::StaticClass, TEXT("/Script/AC2019S2"), TEXT("AEmitter_AC"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEmitter_AC);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
