// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AC2019S2_Emitter_AC_generated_h
#error "Emitter_AC.generated.h already included, missing '#pragma once' in Emitter_AC.h"
#endif
#define AC2019S2_Emitter_AC_generated_h

#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_RPC_WRAPPERS
#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEmitter_AC(); \
	friend struct Z_Construct_UClass_AEmitter_AC_Statics; \
public: \
	DECLARE_CLASS(AEmitter_AC, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AC2019S2"), NO_API) \
	DECLARE_SERIALIZER(AEmitter_AC)


#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEmitter_AC(); \
	friend struct Z_Construct_UClass_AEmitter_AC_Statics; \
public: \
	DECLARE_CLASS(AEmitter_AC, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AC2019S2"), NO_API) \
	DECLARE_SERIALIZER(AEmitter_AC)


#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEmitter_AC(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEmitter_AC) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEmitter_AC); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEmitter_AC); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEmitter_AC(AEmitter_AC&&); \
	NO_API AEmitter_AC(const AEmitter_AC&); \
public:


#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEmitter_AC(AEmitter_AC&&); \
	NO_API AEmitter_AC(const AEmitter_AC&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEmitter_AC); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEmitter_AC); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEmitter_AC)


#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_PRIVATE_PROPERTY_OFFSET
#define AC2019S2_Source_AC2019S2_Emitter_AC_h_9_PROLOG
#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_PRIVATE_PROPERTY_OFFSET \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_RPC_WRAPPERS \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_INCLASS \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AC2019S2_Source_AC2019S2_Emitter_AC_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_PRIVATE_PROPERTY_OFFSET \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_INCLASS_NO_PURE_DECLS \
	AC2019S2_Source_AC2019S2_Emitter_AC_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AC2019S2_API UClass* StaticClass<class AEmitter_AC>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AC2019S2_Source_AC2019S2_Emitter_AC_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
