// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AC2019S2_AC2019S2GameModeBase_generated_h
#error "AC2019S2GameModeBase.generated.h already included, missing '#pragma once' in AC2019S2GameModeBase.h"
#endif
#define AC2019S2_AC2019S2GameModeBase_generated_h

#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_RPC_WRAPPERS
#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAC2019S2GameModeBase(); \
	friend struct Z_Construct_UClass_AAC2019S2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAC2019S2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AC2019S2"), NO_API) \
	DECLARE_SERIALIZER(AAC2019S2GameModeBase)


#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAC2019S2GameModeBase(); \
	friend struct Z_Construct_UClass_AAC2019S2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAC2019S2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AC2019S2"), NO_API) \
	DECLARE_SERIALIZER(AAC2019S2GameModeBase)


#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAC2019S2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAC2019S2GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAC2019S2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAC2019S2GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAC2019S2GameModeBase(AAC2019S2GameModeBase&&); \
	NO_API AAC2019S2GameModeBase(const AAC2019S2GameModeBase&); \
public:


#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAC2019S2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAC2019S2GameModeBase(AAC2019S2GameModeBase&&); \
	NO_API AAC2019S2GameModeBase(const AAC2019S2GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAC2019S2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAC2019S2GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAC2019S2GameModeBase)


#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_12_PROLOG
#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_RPC_WRAPPERS \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_INCLASS \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AC2019S2_API UClass* StaticClass<class AAC2019S2GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AC2019S2_Source_AC2019S2_AC2019S2GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
