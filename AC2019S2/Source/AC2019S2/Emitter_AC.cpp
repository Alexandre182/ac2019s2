// Fill out your copyright notice in the Description page of Project Settings.


#include "Emitter_AC.h"
#include "Engine/World.h"
#include "Particle.h"

// Sets default values
AEmitter_AC::AEmitter_AC()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEmitter_AC::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("Location: %s"), *GetActorLocation().ToString());
		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;

			FRotator Rotator;

			FVector SpawnLocation = this->GetActorLocation();

			World->SpawnActor<AParticle>(Particle, SpawnLocation, Rotator, SpawnParams);
		}
}

// Called every frame
void AEmitter_AC::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

