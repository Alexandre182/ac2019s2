// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AC2019S2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class AC2019S2_API AAC2019S2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
