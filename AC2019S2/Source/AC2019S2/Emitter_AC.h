// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Emitter_AC.generated.h"

UCLASS()
class AC2019S2_API AEmitter_AC : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEmitter_AC();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Emission")
	int32 Count;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Emission")
	TSubclassOf<class AParticle> Particle;

};
